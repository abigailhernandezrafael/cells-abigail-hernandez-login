{
  const {
    html,
  } = Polymer;
  /**
    `<cells-abigailhernandez-login>` Description.

    Example:

    ```html
    <cells-abigailhernandez-login></cells-abigailhernandez-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-abigailhernandez-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsAbigailhernandezLogin extends Polymer.Element {

    static get is() {
      return 'cells-abigailhernandez-login';
    }

    static get properties() {
      return {
        uss: {
          type: String,
          value: ''
        },
        contrasena: {
          type: String,
          value: ''
        },
        prop4: {
          type: Boolean,
          value: false,
          notify: true
        }
      };
    }

    validar(puss, contrasena){
      if(this.uss=="esly" && this.contrasena=="12345"){
        //cambiar valor a prop4
        this.set('prop4', true);
        //pintar en consola el valor que recibe
        //lanzar evento
        this.dispatchEvent(new CustomEvent('login-success',{detail:(this.uss,this.contrasena)}));
        console.log(this.prop4);
      }else{
        this.dispatchEvent(new CustomEvent('login-error',{detail:(this.uss,this.contrasena)}));
        alert('Usuario y/o contraseña incorectos');
      }
    }

    static get template() {
      return html `
      <style include="cells-abigailhernandez-login-styles cells-abigailhernandez-login-shared-styles"></style>
      <slot></slot>
      <div>
        <label>Usuario</label>
        <input type="text" name="usuario" value="{{uss::input}}"/>
    </div>
    <div>
        <label>Contraseña</label>
        <input type="password" value="{{contrasena::input}}" name="contraseña" />
    </div>
    <div>
        <button name="favorito" type="button" on-click="validar">Entrar</button>
    </div>
          <p>Welcome to Cells</p>
      
      `;
    }
  }

  customElements.define(CellsAbigailhernandezLogin.is, CellsAbigailhernandezLogin);
}